const mongoose = require('mongoose');
const config = require('./config');
const Category = require("./models/Category");
const Item = require("./models/Item");
const User = require("./models/User");

const run = async () => {
    await mongoose.connect(config.db.url, config.db.options);

    const collections = await mongoose.connection.db.listCollections().toArray();

    for(const coll of collections) {
        await mongoose.connection.db.dropCollection(coll.name);
    }

    const [carCategory, fridgeCategory] = await Category.create({
        title: 'car',
        description: 'New car description'
    }, {
        title: 'Fridge',
        description: 'New Fridge description'
    });

    const [user1, user2] =  await User.create({
        username: 'User',
        password: '123',
        token: 'qweqwe',
        displayName: 'Yuri',
        phoneNumber: '12432532432'
    }, {
        username: 'Admin',
        password: '321',
        token: 'ewqewq',
        displayName: 'Ivan',
        phoneNumber: '7564543242'
    });

    await Item.create({
        title: 'BMW',
        description: 'BMW car',
        price: 534232,
        category: carCategory,
        user: user1,
        image: 'fixtures/BMW.jpg'
    }, {
        title: 'SAMSUNG',
        description: 'SAMSUNG fridge',
        price: 5223,
        category: fridgeCategory,
        user: user2,
        image: 'fixtures/SAMSUNG.png'
    });



    await mongoose.connection.close();
}

run().catch(console.error);