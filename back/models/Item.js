const mongoose = require('mongoose');

const ItemSchema = new mongoose.Schema({
    title: {
        type: String,
        required: [true, 'Title can not be empty'],
    },
    description: {
        type: String,
        required: [true, 'Description can not be empty'],
    },
    image: String,
    price: {
        type: Number,
        min: 1,
        required: [true, 'Price can not be empty and less then 0'],
    },
    category: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Category',
        required: true,
    },
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true,
    }
});

const Item = mongoose.model('Item', ItemSchema);
module.exports = Item;