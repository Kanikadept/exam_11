const express = require('express');

const User = require('../models/User');

const router = express.Router();

router.post('/', async (req, res) => {
    try {
        const user = new User(req.body);
        user.generateToken();
        await user.save();
        return res.send(user);
    } catch (error) {
        return res.status(400).send(error);
    }
});


router.post('/sessions', async (req, res) => {
    const user = await User.findOne({username: req.body.username});
    if (!user) {
        return res.status(400).send({message: 'Username not found'});
    }

    const isMatch = await user.checkPassword(req.body.password);

    if (!isMatch) {
        return res.status(401).send({message: 'Password is wrong'})
    }

    user.generateToken();
    const updatedUser = await User.findOneAndUpdate({_id: user._id}, {token: user.token}, {new: true});
    return res.send({message: 'Username and password correct!', updatedUser});
});

module.exports = router;