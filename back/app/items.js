const express = require('express');
const multer = require('multer');
const {nanoid} = require('nanoid');
const path = require("path");
///////////////////////////////////
const config = require('../config');
const auth = require("../myMiddleWare/auth");
const Item = require("../models/Item");


const router = express.Router();

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
})

const upload = multer({storage});

router.post('/', auth, upload.single('image'), async (req, res) => {
    try {
        const itemData = req.body;

        if(req.file) {
            itemData.image = 'uploads/' + req.file.filename;
        }

        itemData.user = req.user;
        const item = new Item(itemData);
        await item.save();
        res.send(item);
    } catch (e) {
        res.status(500).send(e);
    }
});

router.get('/', async (req, res) => {
    try {

        const criteria = {};
        if (req.query.category) {
            criteria.category = req.query.category;
        }

        const itemData = await Item.find(criteria);
        res.send(itemData);
    } catch (e) {
        res.status(500).send(e);
    }
});

router.get('/:id', async (req, res) => {
    try {
        const itemData = await Item.findOne({_id: req.params.id}).populate('user', 'username phoneNumber').populate('category', 'title');
        if (itemData) {
            res.send(itemData);
        }
        res.sendStatus(404);
    } catch (e) {
        res.status(500).send(e);
    }
});

router.delete('/:id', auth, async (req, res) => {
    try {
        const itemData = await Item.findOne({_id: req.params.id});
        if (!itemData) {
            return res.sendStatus(404);
        }

        if (!itemData.user.equals(req.user._id)) {
            return res.status(403).send({error: 'you cant delete others items'});
        }
        const deleted = await Item.findByIdAndRemove(itemData._id);
        res.send(deleted);

    } catch (e) {
        res.status(500).send(e);
    }
});

module.exports = router;


