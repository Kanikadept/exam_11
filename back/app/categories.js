const express = require('express');

////////////////////////////////////
const auth = require("../myMiddleWare/auth");
const Category = require("../models/Category");


const router = express.Router();

router.post('/', auth, async (req, res) => {
    try {

        const categoryData = req.body;

        const category = new Category(categoryData);
        await category.save();
        res.send(category);
    } catch (e) {
        res.status(500).send(e);
    }
});

router.get('/', async (req, res) => {

    try {
        const categoriesData = await Category.find();
        res.send(categoriesData);
    } catch (e) {
        res.status(500).send(e);
    }
});

router.get('/:id', async (req, res) => {
    try {
        const categoryData = await Category.findOne({_id: req.params.id});
        if (categoryData) {
            res.send(categoryData);
        }
        res.sendStatus(404);
    } catch (e) {
        res.status(500).send(e);
    }
});

module.exports = router;


