import React, {useEffect} from 'react';
import './Navigation.css';
import {useDispatch, useSelector} from "react-redux";
import {fetchCategories} from "../../../store/actions/categoriesActions";
import {fetchItems} from "../../../store/actions/itemsActions";

const Navigation = () => {

    const dispatch = useDispatch();
    const categories = useSelector(state => state.categories.categories);

    useEffect(() => {
        dispatch(fetchCategories());
    }, [dispatch])


    const handleSelectCategory = category => {
        dispatch(fetchItems(category));
    }

    const handleSelectAllCategories = () => {
        dispatch(fetchItems());
    }

    return (
        <div className="navigation">
            <ul className="category-list">
                <li onClick={handleSelectAllCategories}>All items</li>
                {categories.map(category => {
                    return <li onClick={() => handleSelectCategory(category._id)} key={category._id}>{category.title}</li>
                })}
            </ul>
        </div>
    );
};

export default Navigation;