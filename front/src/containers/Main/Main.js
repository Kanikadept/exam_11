import React from 'react';
import Navigation from "./Navigation/Navigation";
import Items from "./Items/Items";
import './Main.css';

const Main = () => {
    return (
        <div className="main">
            <Navigation />
            <Items />
        </div>
    );
};

export default Main;