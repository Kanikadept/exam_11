import React, {useEffect} from 'react';
import './SingleItem.css';
import {useDispatch, useSelector} from "react-redux";
import {deleteItem, fetchSingleItem} from "../../../../store/actions/itemsActions";

const SingleItem = props => {

    const dispatch = useDispatch();
    const item = useSelector(state => state.items.item);
    const user = useSelector(state => state.users.user);

    useEffect(() => {
        dispatch(fetchSingleItem(props.match.params.id));
    }, [props.match.params.id, dispatch]);

    const handleDeleteItem = () => {
        dispatch(deleteItem(item._id, user.token));
        props.history.push('/');
    }

    return (item &&
        <div className="singleItem">
            <div className="singleItem__image-wrapper">
                <img src={'http://localhost:8000/'+ item.image} alt=""/>
            </div>
            <span>Title: {item.title}</span>
            <span>Description: {item.description}</span>
            <span>Category: {item.category.title}</span>
            <span>Product owner: {item.user.username}, Phone number: {item.user.phoneNumber}</span>
            <button onClick={handleDeleteItem}>Delete item</button>
        </div>
    );
};

export default SingleItem;