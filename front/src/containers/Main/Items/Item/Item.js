import React from 'react';
import './Item.css';
import {NavLink} from "react-router-dom";

const Item = ({id, image, title, price}) => {
    return (
        <NavLink to={'/item/'+ id} className="item">
            <div className="item__image-wrapper">
                <img className="item__image" src={'http://localhost:8000/' + image} alt=""/>
            </div>
            <span className="title">{title}</span>
            <span className="price">{price} som</span>
        </NavLink>
    );
};

export default Item;