import React, {useEffect} from 'react';
import './Items.css';
import {useDispatch, useSelector} from "react-redux";
import Item from "./Item/Item";
import {fetchItems} from "../../../store/actions/itemsActions";

const Items = () => {

    const dispatch = useDispatch();
    const items = useSelector(state => state.items.items);

    useEffect(() => {
        dispatch(fetchItems());
    }, [dispatch]);

    return (
        <div className="items">
            <span>All items</span>
            <div className="items__wrapper">

                {items.map(item => {
                    return <Item key={item._id} id={item._id} price={item.price} image={item.image} title={item.title}/>
                })}
            </div>
        </div>
    );
};

export default Items;