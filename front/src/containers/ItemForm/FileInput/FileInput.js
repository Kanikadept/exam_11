import React, {useRef, useState} from 'react';
import './FileInput.css';

const FileInput = ({onChange, name, label}) => {

    const inputRef = useRef();

    const [fileName, setFileName] = useState('');

    const activateInput = () => {
        inputRef.current.click();
    }

    const onFileChange = e => {
        if (e.target.files[0]) {
            setFileName(e.target.files[0].name);
        } else {
            setFileName('');
        }

        onChange(e);
    }

    return (
        <>
            <input
                onChange={onFileChange}
                ref={inputRef}
                className="defInput"
                type="file"
                name={name}/>
            <div className="custom-input">
                <div className="input-wrapper" onClick={activateInput}>
                    <input value={fileName} type="text" disabled placeholder={label}/>
                </div>
                <button type={"button"} onClick={activateInput}>Browse</button>
            </div>

        </>
    );
};

export default FileInput;