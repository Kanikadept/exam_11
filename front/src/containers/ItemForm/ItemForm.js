import React, {useEffect, useState} from 'react';
import './ItemForm.css';
import FileInput from "./FileInput/FileInput";
import {useDispatch, useSelector} from "react-redux";
import {fetchCategories} from "../../store/actions/categoriesActions";
import {createItem} from "../../store/actions/itemsActions";

const ItemForm = props => {
    const dispatch = useDispatch();
    const user = useSelector(state => state.users.user);
    const categories = useSelector(state => state.categories.categories);

    const [item, setItem] = useState({
        title: '',
        description: '',
        price: 0,
        image: '',
        category: '',
    });

    useEffect(() => {
        if(!user) {
            props.history.push('/');
        }
    }, [user, props.history]);

    useEffect(() => {
        dispatch(fetchCategories());
    }, [dispatch]);



    const handleChange = (event) => {
        const {name, value} = event.target;
        setItem(prevState => ({
            ...prevState, [name]: value
        }));
    };

    const fileChangeHandler = e => {
        const name = e.target.name;
        const file = e.target.files[0];

        setItem(prevSate => ({
            ...prevSate,
            [name]: file
        }));
    };

    const handleSubmit = (event) => {
        event.preventDefault();
        const formData = new FormData();
        Object.keys(item).forEach(key => {
            formData.append(key, item[key]);
        });
        dispatch(createItem(formData, user.token));
        props.history.push('/');
    }

    return (
        <form onSubmit={handleSubmit} className="post-form">
            <div className="form-row">
                <label>Title</label>
                <input name="title" value={item.title} onChange={handleChange} type="text"/>
            </div>
            <div className="form-row">
                <label>Description</label>
                <input name="description" value={item.description} onChange={handleChange} type="text"/>
            </div>
            <div className="form-row">
                <label>Price</label>
                <input name="price" value={item.price} onChange={handleChange} type="number" min="1"/>
            </div>
            <div className="form-row">
                <label>Image</label>
                <FileInput name="image" label="Image" onChange={fileChangeHandler}/>
            </div>
            <div className="form-row">
                <label>Category</label>

                <select name="category" id="category" onChange={handleChange} value={item.category}>
                    {categories.map(category => {
                        return <option key={category._id}  value={category._id}>{category.title}</option>
                    })}
                </select>
                {console.log(item.category, '++++')}
            </div>
            <button>Create item</button>
        </form>
    );
};

export default ItemForm;