import React from 'react';
import {NavLink} from "react-router-dom";

import './UserMenu.css';
import {useDispatch} from "react-redux";
import {logoutUser} from "../../../store/actions/usersActions";

const UserMenu = ({user}) => {

    const dispatch = useDispatch();


    return (
        <div className="user-menu">
            <span>Hello, {user.username}</span>
            <NavLink to="/itemForm">Add new item</NavLink> or <NavLink to="/">
            <button onClick={() => dispatch(logoutUser())}>Logout</button></NavLink>
        </div>
    );
};

export default UserMenu;