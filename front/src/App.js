import {Route, Switch} from 'react-router-dom';
import Layout from "./components/Layout/Layout";
import Register from "./containers/Register/Register";
import LogIn from "./containers/Login/LogIn";
import './App.css';
import Main from "./containers/Main/Main";
import ItemForm from "./containers/ItemForm/ItemForm";
import SingleItem from "./containers/Main/Items/SingleItem/SingleItem";

const App = () => (
    <div className="App">
        <div className="container">
            <Layout>
                <Switch>
                    <Route path="/register" component={Register} exact/>
                    <Route path="/login" component={LogIn} exact/>
                    <Route path="/" component={Main} exact/>
                    <Route path="/itemForm" component={ItemForm} exact/>
                    <Route path="/item/:id" component={SingleItem} exact/>
                </Switch>
            </Layout>
        </div>
    </div>
);

export default App;
