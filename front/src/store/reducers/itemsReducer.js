import {
    CREATE_ITEM, DELETE_ITEM_SUCCESS,
    FETCH_ITEMS_FAILURE,
    FETCH_ITEMS_REQUEST,
    FETCH_ITEMS_SUCCESS,
    FETCH_SINGLE_ITEM_SUCCESS
} from "../actions/itemsActions";

const initialState = {
    items: [],
    item: null,
    loadingItems: false,
    errorItems: null,
}

const itemsReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_ITEMS_REQUEST:
            return {...state, loadingItems: true};
        case FETCH_ITEMS_SUCCESS:
            return {...state, items: action.payload, loadingItems: false};
        case CREATE_ITEM:
            return {...state, items: [action.payload, ...state.items]};
        case FETCH_SINGLE_ITEM_SUCCESS:
            return {...state, item: action.payload};
        case DELETE_ITEM_SUCCESS:
            const itemsCopy = [...state.items];
            const removeIndex = itemsCopy.map(item => {
                return item._id;
            }).indexOf(action.payload);
            itemsCopy.slice(removeIndex, 1);
            return {...state, items: itemsCopy};
        case FETCH_ITEMS_FAILURE:
            return {...state, errorItems: action.payload, loadingItems: false};
        default:
            return state;
    }
};

export default itemsReducer;