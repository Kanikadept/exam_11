import {
    FETCH_CATEGORIES_FAILURE,
    FETCH_CATEGORIES_REQUEST,
    FETCH_CATEGORIES_SUCCESS
} from "../actions/categoriesActions";

const initialState = {
    categories: [],
    loadingCategories: false,
    errorCategories: null,
}

const categoriesReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_CATEGORIES_REQUEST:
            return {...state, loadingCategories: true};
        case FETCH_CATEGORIES_SUCCESS:
            return {...state, categories: action.payload, loadingCategories: false};
        case FETCH_CATEGORIES_FAILURE:
            return {...state, errorCategories: action.payload, loadingCategories: false};
        default:
            return state;
    }
}

export default categoriesReducer;