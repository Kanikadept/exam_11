import axiosApi from "../../axiosApi";

export const FETCH_CATEGORIES_REQUEST = 'FETCH_CATEGORIES_REQUEST';
export const fetchCategoriesRequest = () => ({type: FETCH_CATEGORIES_REQUEST});

export const FETCH_CATEGORIES_FAILURE = 'FETCH_CATEGORIES_FAILURE';
export const fetchCategoriesFailure = payload => ({type: FETCH_CATEGORIES_FAILURE, payload});

export const FETCH_CATEGORIES_SUCCESS = 'FETCH_CATEGORIES_SUCCESS';
export const fetchCategoriesSuccess = payload => ({type: FETCH_CATEGORIES_SUCCESS, payload});
export const fetchCategories = () => {
    return async dispatch => {
        try {
            const categoriesResponse = await axiosApi.get('/categories');
            dispatch(fetchCategoriesSuccess(categoriesResponse.data));
        } catch (err) {
            if(err.response && err.response.data) {
                dispatch(fetchCategoriesFailure(err.response.data));
            } else {
                dispatch(fetchCategoriesFailure({global: 'No internet'}));
            }
        }
    }
}