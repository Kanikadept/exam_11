import axiosApi from "../../axiosApi";

export const FETCH_ITEMS_REQUEST = 'FETCH_ITEMS_REQUEST';
export const fetchItemsRequest = () => ({type: FETCH_ITEMS_REQUEST});

export const FETCH_ITEMS_FAILURE = 'FETCH_ITEMS_FAILURE';
export const fetchItemsFailure = payload => ({FETCH_ITEMS_FAILURE, payload});

export const FETCH_ITEMS_SUCCESS = 'FETCH_ITEMS_SUCCESS';
export const fetchItemsSuccess = payload => ({type: FETCH_ITEMS_SUCCESS, payload});
export const fetchItems = category => {
    return async dispatch => {
        try {
            const itemsResponse = await axiosApi.get('/items', {params: {category: category}});
            dispatch(fetchItemsSuccess(itemsResponse.data));
        } catch (err) {
            if(err.response && err.response.data) {
                dispatch(fetchItemsFailure(err.response.data));
            } else {
                dispatch(fetchItemsFailure({global: 'No internet'}));
            }
        }
    }
}

export const CREATE_ITEM = 'CREATE_ITEM';
export const createItemSuccess = payload => ({type: CREATE_ITEM, payload});
export const createItem = (payload, token) => {
    return async dispatch => {
        try {
            const itemResponse = await axiosApi.post('/items', payload, {headers: {Authorization: token}});
            dispatch(createItemSuccess(itemResponse.data));
        } catch (err) {
            console.log(err);
        }
    }
}

export const FETCH_SINGLE_ITEM_SUCCESS = 'FETCH_SINGLE_ITEM';
export const fetchSingleItemSuccess = payload => ({type: FETCH_SINGLE_ITEM_SUCCESS, payload});
export const fetchSingleItem = itemId => {
    return async dispatch => {
        try {
            const itemResponse = await axiosApi.get('/items/' + itemId);
            dispatch(fetchSingleItemSuccess(itemResponse.data));
        } catch (err) {
            console.log(err);
        }
    }
}

export const DELETE_ITEM_SUCCESS = 'DELETE_ITEM_SUCCESS';
export const deleteItemSuccess = payload => ({type: DELETE_ITEM_SUCCESS, payload});
export const deleteItem = (itemId, token) => {
    return async dispatch => {
        try {
            console.log(token, 'toke------')
            const itemResponse = await axiosApi.delete('/items/'+ itemId, {headers: {Authorization: token}});
            dispatch(deleteItemSuccess(itemResponse.data._id));
        } catch (err) {
            console.log(err);
        }
    }
}